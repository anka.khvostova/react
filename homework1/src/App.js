
import { PureComponent } from 'react';
import './App.css';
import Modal from "./Components/Modal/Modal";
import {headerProps} from "./Props";
import Header from './Components/Header/Header';
class App extends PureComponent {
  render(){
      return (
    <div className="App">
   <Header properties={headerProps}/>
    </div>
  );
  }

}

export default App;
