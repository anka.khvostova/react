import { PureComponent } from "react";

class Button extends PureComponent{
  render(){
      const{text,clickEvent, styleClass} =this.props;
      return(
          <>
          <button className={styleClass} onClick={clickEvent}>{text}</button>
          </>
      )
  }

}
export default Button;