import { PureComponent } from 'react';
import Button from '../Button/Button';

class Modal extends PureComponent{
    constructor(props){
         super(props);
         this.state={
             btn1Text:"OK",
             btn2Text:"Cancel",
             visibility:this.props.show,

         };
    }
   
   closeWindow =()=>{
    this.setState({visibility: false});
    this.props.update(false);
    // document.getElementById('modalOverlay').remove();
   
   }

   sayHello =()=>{
    this.setState({visibility: false});  
    this.props.update(false);
    setTimeout(()=>{alert('Hello! Welcome!')}, 300);
   }
   closeModalOverlay=(e)=>{
      
        //   e.stopPropagation();
    this.setState({visibility: false});
    this.props.update(false);    
       }
    // modalStopPropagation=(e)=>{
    //        e.stopPropagation();   
    //    }
   
   
   
render(){
    const {properties,visibility} = this.props;
    const{modalOverlayStyle,modalStyle,titleModalText,modalText,titleStyle,textStyle,btnStyle,exitBtn=false,exitBtnStyle}=properties;
   
    this.setState({visibility:visibility});
    let btn;
       
         if(this.state.visibility){
         console.log('show');  
           if(exitBtn){
            btn = <Button text="X" styleClass={exitBtnStyle} clickEvent={this.closeWindow}/>
        } 
            return(
            
                <div className={modalOverlayStyle} id="modalOverlay" onClick={this.closeModalOverlay}>
                    <div className={modalStyle} onClick={(e)=>{e.stopPropagation();}}>
                    <h2 className={titleStyle}>
                        {titleModalText}
                        {btn}
                    </h2>
                    <p className={textStyle}>{modalText}</p>
                    <Button styleClass={btnStyle} text={this.state.btn1Text} clickEvent={this.sayHello}/>
                    <Button  styleClass={btnStyle} text={this.state.btn2Text} clickEvent={this.closeWindow}/>

                </div> 
                </div>
    )
}
    else{
        console.log('close');
        return(<></>)
    }
}

}
export default Modal;