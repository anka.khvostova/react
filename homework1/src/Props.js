export const modalProps1={
    titleModalText:'Привет, я "Первое" модальное окно!',
    modalText:'Дорогу осилит идущий',
    modalOverlayStyle:'modal-overlay',
    modalStyle:'modal',
    titleStyle:"modal-title btn1",
    textStyle:"modal-text",
    btnStyle:"btn-modal btn1",
    exitBtn:true,
    exitBtnStyle:"exit-btn btn2",


};
export const modalProps2={
    titleModalText:'Привет, я "Второе" модальное окно!',
    modalText:'Терпение и труд всё перетрут!',
    modalOverlayStyle:'modal-overlay',
    modalStyle:'modal',
    titleStyle:"modal-title btn2",
    textStyle:"modal-text",
    btnStyle:"btn-modal btn2",
    exitBtn:true,
    exitBtnStyle:"exit-btn btn1",


};

export const headerProps ={
    modal1:modalProps1,
    modal2:modalProps2,
    headerStyle:'',
    btn1:{
        text:'Кнопка 1',
        styleClass:'btn btn1',
    },
    btn2:{
        text:'Кнопка 2',
        styleClass:'btn btn2',
    }

};